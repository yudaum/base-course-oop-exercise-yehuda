package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionCompatibilities.BdaMissionCompatible;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private final String objective;
    private BdaMissionCompatible vehicle;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String objective) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, aerialVehicle);
        this.objective = objective;
    }

    @Override
    protected void checkValidity() throws AerialVehicleNotCompatibleException {
        try {
            vehicle = (BdaMissionCompatible) aerialVehicle;
        } catch (Exception e) {
            throw new AerialVehicleNotCompatibleException(e.getMessage());
        }
    }

    @Override
    public String executeMission() {
        return pilotName + ": " + aerialVehicle.getName() + " " + objective + " with: " + vehicle.getCameraType();
    }
}
