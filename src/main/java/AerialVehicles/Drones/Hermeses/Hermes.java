package AerialVehicles.Drones.Hermeses;

import AerialVehicles.Drones.Drone;
import AerialVehicles.MissionCompatibilities.BdaMissionCompatible;
import AerialVehicles.Tools.Cameras.CameraType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public abstract class Hermes extends Drone implements BdaMissionCompatible {
    private final CameraType cameraType;

    public Hermes(Coordinates srcPlace, CameraType cameraType, SensorType sensorType) {
        super(srcPlace, sensorType);
        MAX_HOURS_SINCE_REPAIR = 100;
        this.cameraType  = cameraType;
    }

    @Override
    public CameraType getCameraType() {
        return cameraType;
    }
}