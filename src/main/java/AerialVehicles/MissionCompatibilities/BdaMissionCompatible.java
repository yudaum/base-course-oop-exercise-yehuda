package AerialVehicles.MissionCompatibilities;

import AerialVehicles.Tools.Cameras.CameraType;

public interface BdaMissionCompatible {
    CameraType getCameraType();
}
