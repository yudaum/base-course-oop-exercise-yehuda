package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionCompatibilities.IntelligenceMissionCompatible;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private final String region;
    private IntelligenceMissionCompatible vehicle;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String region) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, aerialVehicle);
        this.region = region;
    }

    @Override
    protected void checkValidity() throws AerialVehicleNotCompatibleException {
        try {
            vehicle = (IntelligenceMissionCompatible) aerialVehicle;
        } catch (Exception e) {
            throw new AerialVehicleNotCompatibleException(e.getMessage());
        }
    }

    @Override
    public String executeMission() {
        return pilotName + ": " + aerialVehicle.getName() + " Collecting Data in " + region +
                " with: sensor type: " + vehicle.getSensorType();
    }
}
