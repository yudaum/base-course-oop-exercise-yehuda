package AerialVehicles.Drones.Harons;

import AerialVehicles.Tools.Missiles.MissileType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public class Eitan extends Haron{

    public Eitan(Coordinates srcPlace, MissileType missileType, int numOfMissiles, SensorType sensorType) {
        super(srcPlace, missileType, numOfMissiles, sensorType);
    }
}
