package AerialVehicles.FighterJets;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionCompatibilities.AttackMissionCompatible;
import AerialVehicles.Tools.Missiles.MissileType;
import Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle implements AttackMissionCompatible {
    private static final int DISTANCE_PER_HOUR = 10;
    private final MissileType missileType;
    private final int numOfMissiles;

    public FighterJet(Coordinates srcPlace, MissileType mt, int nom) {
        super(srcPlace);
        MAX_HOURS_SINCE_REPAIR = 250;
        missileType = mt;
        numOfMissiles = nom;
    }

    @Override
    protected int calculateFlightTime(Coordinates currentPlace, Coordinates destination) {
        double distance = currentPlace.getDistanceFrom(destination);
        return (int) (distance / DISTANCE_PER_HOUR);
    }

    @Override
    public MissileType getMissileType() {
        return missileType;
    }

    @Override
    public int getNumOfMissiles() {
        return numOfMissiles;
    }
}
