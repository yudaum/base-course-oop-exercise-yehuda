package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
    protected Coordinates destination;
    protected String pilotName;
    protected AerialVehicle aerialVehicle;

    public Mission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle) throws AerialVehicleNotCompatibleException {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
        checkValidity();
    }

    protected abstract void checkValidity() throws AerialVehicleNotCompatibleException;

    public abstract String executeMission();

    public void begin() {
        aerialVehicle.flyTo(destination);
        System.out.println("Beginning Mission!");
    }

    public void cancel() {
        aerialVehicle.land(aerialVehicle.getSrcPlace());
        System.out.println("Abort Mission!");
    }

    public void finish() {
        executeMission();
        aerialVehicle.land(aerialVehicle.getSrcPlace());
        System.out.println("Finish Mission!");
    }
}
