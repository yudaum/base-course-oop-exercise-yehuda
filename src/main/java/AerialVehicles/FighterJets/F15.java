package AerialVehicles.FighterJets;


import AerialVehicles.MissionCompatibilities.IntelligenceMissionCompatible;
import AerialVehicles.Tools.Missiles.MissileType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public class F15 extends FighterJet implements IntelligenceMissionCompatible {
    private final SensorType sensorType;

    public F15(Coordinates srcPlace, MissileType mt, int nom, SensorType sensorType) {
        super(srcPlace, mt, nom);
        this.sensorType = sensorType;
    }

    @Override
    public SensorType getSensorType() {
        return sensorType;
    }
}
