package AerialVehicles.Drones.Harons;


import AerialVehicles.MissionCompatibilities.BdaMissionCompatible;
import AerialVehicles.Tools.Cameras.CameraType;
import AerialVehicles.Tools.Missiles.MissileType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public class Shoval extends Haron implements BdaMissionCompatible {
    private final CameraType cameraType;

    public Shoval(Coordinates srcPlace, MissileType missileType, int numOfMissiles, CameraType cameraType, SensorType sensorType) {
        super(srcPlace, missileType, numOfMissiles, sensorType);
        this.cameraType = cameraType;
    }

    @Override
    public CameraType getCameraType() {
        return cameraType;
    }
}

