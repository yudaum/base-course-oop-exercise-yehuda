import AerialVehicles.Drones.Hermeses.Zik;
import AerialVehicles.FighterJets.F15;
import AerialVehicles.FighterJets.F16;
import AerialVehicles.Tools.Cameras.CameraType;
import AerialVehicles.Tools.Missiles.MissileType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;

public class Scenarios {
    public static void main(String[] args) {
        scenario_1();
        scenario_2();
        scenario_3();
    }

    private static void scenario_1() {
        Coordinates jetSrc = new Coordinates(0.0,0.0);
        Coordinates jetDest = new Coordinates(0.0,30000.0);

        F15 f15 = new F15(jetSrc, MissileType.Spice250, 10, SensorType.InfraRed);
        f15.flyTo(jetDest);
        f15.land(jetDest);  // fly to a far place

        f15.flyTo(jetSrc); // tries to fly back, should work only if repaired properly
        f15.land(jetSrc);
    }

    private static void scenario_2() {
        Coordinates droneSrc = new Coordinates(0.0,0.0);
        Coordinates dest = new Coordinates(21.0,20.0);
        String pilotName = "Mr. Pilot";
        
        Zik zik = new Zik(droneSrc, CameraType.NightVision, SensorType.InfraRed);
        try {
            AttackMission attackMission = new AttackMission(dest, pilotName, zik, "try to catch exception");  // should raise an exception
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println("This exception should've been called, well detected!");
        }
    }

    private static void scenario_3() {
        Coordinates droneSrc = new Coordinates(0.0,0.0);
        Coordinates dest = new Coordinates(21.0,20.0);
        String pilotName = "Mr. Pilot";

        F16 f16 = new F16(droneSrc, MissileType.Amram, 10, CameraType.NightVision);
        AttackMission attackMission = null;
        try {
            attackMission = new AttackMission(dest, pilotName, f16, "kill enemies or something");  // should work just fine
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();  // shouldn't happen
        }
        
        String msg = attackMission.executeMission();
        System.out.println(msg);  // should print "Mr. Pilot: F16 Attacking kill enemies or something with: AmramX10"

    }
}
