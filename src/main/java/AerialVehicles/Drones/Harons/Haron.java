package AerialVehicles.Drones.Harons;

import AerialVehicles.Drones.Drone;
import AerialVehicles.MissionCompatibilities.AttackMissionCompatible;
import AerialVehicles.Tools.Missiles.MissileType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public abstract class Haron extends Drone implements AttackMissionCompatible {
    private final MissileType missileType;
    private final int numOfMissiles;

    public Haron(Coordinates srcPlace, MissileType missileType, int numOfMissiles, SensorType sensorType) {
        super(srcPlace, sensorType);
        MAX_HOURS_SINCE_REPAIR = 150;
        this.missileType = missileType;
        this.numOfMissiles = numOfMissiles;
    }


    @Override
    public MissileType getMissileType() {
        return missileType;
    }

    @Override
    public int getNumOfMissiles() {
        return numOfMissiles;
    }
}
