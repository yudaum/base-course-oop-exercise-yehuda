package AerialVehicles.MissionCompatibilities;

import AerialVehicles.Tools.Sensors.SensorType;

public interface IntelligenceMissionCompatible {
    SensorType getSensorType();
}
