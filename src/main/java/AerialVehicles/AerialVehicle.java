package AerialVehicles;


import Entities.Coordinates;

public abstract class AerialVehicle {

    protected enum FlightStatus {
        READY,
        NOT_READY,
        IN_AIR
    }

    private final Coordinates srcPlace;
    protected static int MAX_HOURS_SINCE_REPAIR;
    private int hoursSinceRepair = 0; // number of flight hours since last repair
    protected FlightStatus flightStatus = FlightStatus.READY;
    private Coordinates currentPlace;

    public AerialVehicle(Coordinates srcPlace) {
        this.srcPlace = srcPlace;
        currentPlace = srcPlace;
    }


    public void flyTo(Coordinates destination) {
        if (flightStatus != FlightStatus.READY) {
            System.out.println("Aerial Vehicle isn't ready to fly");
        } else {
            System.out.println("Flying to " + destination.getLongitude() + ", " + destination.getLatitude());
            flightStatus = FlightStatus.IN_AIR;
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on " + destination.getLongitude() + ", " + destination.getLatitude());
        hoursSinceRepair += calculateFlightTime(currentPlace, destination);

        currentPlace = destination;
        check();
    }

    protected abstract int calculateFlightTime(Coordinates currentPlace, Coordinates destination);

    public void check() {
        if (hoursSinceRepair > MAX_HOURS_SINCE_REPAIR) {
            flightStatus = FlightStatus.NOT_READY;
            repair();
        } else {
            flightStatus = FlightStatus.READY;
        }
    }

    public void repair() {

        flightStatus = FlightStatus.READY;
        hoursSinceRepair = 0;
    }

    /**
     * get a String of the vehicle's name
     */
    public String getName() {
        return getClass().getSimpleName();
    }

    public Coordinates getSrcPlace() {
        return srcPlace;
    }
}
