package AerialVehicles.Tools.Cameras;

public enum CameraType {
    Regular,
    Thermal,
    NightVision
}