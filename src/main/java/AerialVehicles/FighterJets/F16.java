package AerialVehicles.FighterJets;


import AerialVehicles.MissionCompatibilities.BdaMissionCompatible;
import AerialVehicles.Tools.Cameras.CameraType;
import AerialVehicles.Tools.Missiles.MissileType;
import Entities.Coordinates;

public class F16 extends FighterJet implements BdaMissionCompatible {
    private final CameraType cameraType;

    public F16(Coordinates srcPlace, MissileType mt, int nom, CameraType cameraType) {
        super(srcPlace, mt, nom);
        this.cameraType = cameraType;
    }

    @Override
    public CameraType getCameraType() {
        return cameraType;
    }
}
