package AerialVehicles.Drones;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionCompatibilities.IntelligenceMissionCompatible;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public abstract class Drone extends AerialVehicle implements IntelligenceMissionCompatible {
    private static final int DISTANCE_PER_HOUR = 1;
    private final SensorType sensorType;

    public Drone(Coordinates srcPlace, SensorType sensorType) {
        super(srcPlace);
        this.sensorType = sensorType;
    }

    public String hoverOverLocation(Coordinates destination) {
        String coord = destination.getLongitude() + ", " + destination.getLatitude();
        String msg = "Hovering Over " + coord;
        flightStatus = FlightStatus.IN_AIR;
        System.out.println(msg);
        return msg;
    }

    @Override
    protected int calculateFlightTime(Coordinates currentPlace, Coordinates destination) {
        double distance = currentPlace.getDistanceFrom(destination);
        return (int) (distance / DISTANCE_PER_HOUR);
    }

    @Override
    public SensorType getSensorType() {
        return sensorType;
    }
}
