package AerialVehicles.Drones.Hermeses;

import AerialVehicles.MissionCompatibilities.AttackMissionCompatible;
import AerialVehicles.Tools.Cameras.CameraType;
import AerialVehicles.Tools.Missiles.MissileType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public class Kochav extends Hermes implements AttackMissionCompatible {

    private final MissileType missileType;
    private final int numOfMissiles;

    public Kochav(Coordinates srcPlace, CameraType cameraType, MissileType missileType, int numOfMissiles, SensorType sensorType) {
        super(srcPlace, cameraType, sensorType);
        this.missileType = missileType;
        this.numOfMissiles = numOfMissiles;
    }

    @Override
    public MissileType getMissileType() {
        return missileType;
    }

    @Override
    public int getNumOfMissiles() {
        return numOfMissiles;
    }
}
