package AerialVehicles.Drones.Hermeses;


import AerialVehicles.Tools.Cameras.CameraType;
import AerialVehicles.Tools.Sensors.SensorType;
import Entities.Coordinates;

public class Zik extends Hermes {
    public Zik(Coordinates srcPlace, CameraType cameraType, SensorType sensorType) {
        super(srcPlace, cameraType, sensorType);
    }
}