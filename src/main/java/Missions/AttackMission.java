package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionCompatibilities.AttackMissionCompatible;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;
    private AttackMissionCompatible vehicle;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String target) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    protected void checkValidity() throws AerialVehicleNotCompatibleException {
        try {
            vehicle = (AttackMissionCompatible) aerialVehicle;
        } catch (Exception e) {
            throw new AerialVehicleNotCompatibleException(e.getMessage());
        }
    }

    @Override
    public String executeMission() {
        return pilotName + ": " + aerialVehicle.getName() + " Attacking " + target + " with: " + vehicle.getMissileType() + "X" + vehicle.getNumOfMissiles();
    }
}
