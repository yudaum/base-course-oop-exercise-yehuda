package AerialVehicles.MissionCompatibilities;

import AerialVehicles.Tools.Missiles.MissileType;

public interface AttackMissionCompatible {
    MissileType getMissileType();

    int getNumOfMissiles();
}
